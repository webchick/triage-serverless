# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/customer_label'
require_relative '../processor/customer_contribution_merged_notifier'
require_relative '../processor/customer_contribution_created_notifier'
require_relative '../processor/deprecated_label'
require_relative '../processor/hackathon_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/merge_request_help'
require_relative '../processor/reactive_labeler'
require_relative '../processor/thank_community_contribution'
require_relative '../processor/jihu_contribution'
require_relative '../processor/type_label'

module Triage
  class Handler
    DEFAULT_TRIAGERS = [
      AvailabilityPriority,
      CustomerLabel,
      TypeLabel,
      BackstageLabel,
      DeprecatedLabel,
      ThankCommunityContribution,
      JiHuContribution,
      DocCommunityContribution,
      MergeRequestHelp,
      ReactiveLabeler,
      CustomerContributionMergedNotifier,
      CustomerContributionCreatedNotifier
    ].freeze

    def initialize(event, triagers: DEFAULT_TRIAGERS)
      @event = event
      @triagers = triagers
    end

    def process
      triagers.map do |triager|
        triager.triage(event)
      end.compact
    end

    private

    attr_reader :event, :triagers
  end
end
