# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class JiHuContribution < Triager
    LABEL_NAME = 'JiHu contribution'

    def applicable?
      event.merge_request? && event.new_entity? && event.jihu_contributor?
    end

    def process
      add_comment(%Q{/label ~"#{LABEL_NAME}"})
    end
  end
end
