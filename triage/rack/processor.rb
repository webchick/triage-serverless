require 'rack'

require_relative '../triage/error'
require_relative '../triage/handler'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  module Rack
    class Processor
      def call(env)
        event = env[:event]
        handler = Handler.new(event)

        puts "Running in dry mode" if Triage.dry_run?

        messages = handler.process

        puts "Incoming webhook event: #{event.inspect}, messages: #{messages.inspect}"

        ::Rack::Response.new([JSON.dump(status: :ok, messages: messages)]).finish
      rescue Triage::ClientError => error
        error_response(error)
      rescue => error
        Raven.tags_context(object_kind: event.class.to_s)
        Raven.extra_context(event: event)
        Raven.capture_exception(error)

        puts "Exception with event: #{event.inspect}, error: #{error.class}: #{error.message}"
        error_response(error, status: 500)
      end

      private

      def error_response(error, status: 400)
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], status).finish
      end

      def dry_run?
        !ENV['DRY_RUN'].nil?
      end
    end
  end
end
