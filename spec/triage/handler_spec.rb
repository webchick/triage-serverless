# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  subject { described_class.new(event, triagers: [triager1, triager2]) }

  let(:event) { double('Triage::Event') }
  let(:triager1) { class_double('Triage::Triager', triage: true) }
  let(:triager2) { class_double('Triage::Triager', triage: nil) }

  describe 'DEFAULT_TRIAGERS' do
    it 'includes all triager implementations' do
      expected = [
        Triage::AvailabilityPriority,
        Triage::CustomerLabel,
        Triage::TypeLabel,
        Triage::BackstageLabel,
        Triage::DeprecatedLabel,
        Triage::ThankCommunityContribution,
        Triage::JiHuContribution,
        Triage::DocCommunityContribution,
        Triage::MergeRequestHelp,
        Triage::ReactiveLabeler,
        Triage::CustomerContributionMergedNotifier,
        Triage::CustomerContributionCreatedNotifier
      ]

      expect(described_class::DEFAULT_TRIAGERS).to eq(expected)
    end
  end

  describe '#process' do
    it 'executes all triagers' do
      subject.process

      expect(triager1).to have_received(:triage).once
      expect(triager2).to have_received(:triage).once
    end

    it 'removes nil result from triager' do
      expect(subject.process.all?).to be_truthy
    end
  end
end
