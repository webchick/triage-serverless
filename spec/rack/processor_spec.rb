# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/processor'

describe Triage::Rack::Processor do
  let(:app)      { described_class.new }
  let(:event)    { double('Triage::Event') }
  let(:env)      { { event: event } }
  let(:handler)  { double('Triage::Handler') }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(Triage::Handler).to receive(:new).with(event).and_return(handler)
    allow(app).to receive(:puts)
  end

  context 'when Triage::ClientError is thrown' do
    it 'returns a 400 error' do
      expect(handler).to receive(:process).and_raise(Triage::ClientError)

      expect(status).to eq(400)
      expect(body).to eq(JSON.dump(status: :error, error: "Triage::ClientError", message: "Triage::ClientError"))
    end
  end

  context 'when an error is thrown' do
    let(:error) { StandardError.new('runtime error') }

    before do
      allow(handler).to receive(:process).and_raise(error)
    end

    it 'returns a 500 error and tags Sentry error with event type' do
      expect(Raven).to receive(:capture_exception).with(error)
      expect(Raven).to receive(:tags_context).with(object_kind: 'RSpec::Mocks::Double')
      expect(Raven).to receive(:extra_context).with(event: event)

      expect(status).to eq(500)
      expect(body).to eq(JSON.dump(status: :error, error: "StandardError", message: "runtime error"))
    end
  end

  context 'when no error is thrown' do
    let(:messages) { %w[foo bar baz] }

    before do
      allow(handler).to receive(:process).and_return(messages)
    end

    it 'returns a 200 error with the messages from the processors' do
      expect(status).to eq(200)
      expect(body).to eq(JSON.dump(status: :ok, messages: messages))
    end
  end
end
